jQuery(function($) {

	var commentForm = $('#comment-user');
	var commentList = $('#article-comments');

	/**
	 * Add comments
	 */
	commentForm.submit(function() {
		var form = $(this);
		var userField = form.find('input[name="CommentForm[user]"]');
		var emailField = form.find('input[name="CommentForm[email]"]');
		var textField = form.find('textarea[name="CommentForm[text]"]');

		var template = "<li class=\"list-group-item\"><span><small>" +
			"<a href=\"${email}\" class=\"user_mail\">${user}</a>" +
			" &nbsp;&#8226;&nbsp; " +
			"<a href=\"#to-comment-form\" class=\"comment_replace\" data-comment-user=\"${user}\">Reply</a>" +
			"</small></span><br><p>${text}</p><br></li>";
		$.template( "commentTemplate", template );


		$.ajax({
			type: "POST",
			url: form.attr('action'),
			data: form.serializeArray(),
			dataType: 'JSON',
			success: function(data){
				if (data.comment !== null && data.errors === null) {
					var commentsData = $('#article-comments');
					commentsData.show();
					$.tmpl( "commentTemplate", data.comment ).appendTo(commentsData);

					clearForm(userField);
					clearForm(emailField);
					clearForm(textField);
					replyUser();

					return true;

				} else {
					if(data.errors !== null) {
						var errors = data.errors;
						if(errors.user) {
							userField.parent()
								.addClass('has-error')
								.find('.help-block-error').html(errors.user[0]);
						}
						if(errors.email) {
							emailField.parent()
								.addClass('has-error')
								.find('.help-block-error').html(errors.email[0]);
						}

					}
					return false;
				}
			}
		});

		return false;
	});

	replyUser();

	/**
	 * Reply of the user comment
	 */
	function replyUser(){
		commentList.find('li').on('click', 'a.comment_replace', function() {
			var link = $(this);
			commentForm.find('textarea[name="CommentForm[text]"]').val('#' + link.data('comment-user') +' ').focus();
		});
	}

	/**
	 * Clean form element
	 * @param selector
	 */
	function clearForm(selector) {
		selector.val('');
		selector.parent().removeClass('has-error');
		selector.parent().removeClass('has-success');
	}

});

