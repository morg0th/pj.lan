<?php
/*
 * @var yii\web\View $this
 * @var app\models\Article $article
 * @var app\models\Comment $comments
 */

use yii\helpers\Html;
use yii\captcha\Captcha;
use yii\bootstrap\ActiveForm;
use app\models\CommentForm;

$this->title = Html::encode($article->title) . ' - Article';
$commentForm = new CommentForm();

$this->registerJsFile('/js/jquery-tmpl-master/jquery.tmpl.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('/js/comments.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

?>

<div class="container">
	<div class="page-header">
		<h1><?= Html::encode($article->title); ?></h1>
	</div>
	<div class="container-fluid"><?= Html::decode($article->text); ?></div>

	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Comments:</h3>
		</div>
		<div class="panel-body">
			<a name="to-comment-form"></a>
			<div class="container-fluid">
				<?php $form = ActiveForm::begin([
					'id'     => 'comment-user',
					'action' => 'article/comment',
				]); ?>
				<?= $form->field($commentForm, 'article_id')->hiddenInput(['value' => $article->id])->label(false) ?>
				<?= $form->field($commentForm, 'user')->textInput() ?>
				<?= $form->field($commentForm, 'email') ?>
				<?= $form->field($commentForm, 'text')->textArea(['rows' => 6]) ?>

				<?= $form->field($commentForm, 'verifyCode')->widget(Captcha::className(), [
					'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
				]) ?>

				<div class="form-group">
					<?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
				</div>
				<?php ActiveForm::end(); ?>
			</div>

			<div class="container-fluid">
				<ul class="list-group" id="article-comments" <?php !$comments ? 'style="display:none;"' : ''; ?>>
					<?php if ($comments) : ?>
						<?php foreach ($comments as $item): ?>
							<li class="list-group-item">
								<span>
									<small>
										<a href="mailto:<?= Html::encode($item->email); ?>" class="user_mail">
											<?= Html::encode($item->user); ?>
										</a>&nbsp;&#8226;&nbsp;<a href="#to-comment-form"
										class="comment_replace"
										data-comment-user="<?= Html::encode($item->user); ?>">
										Reply
										</a>
									</small>
								</span>
								<br>
								<p><?= Html::decode($item->text); ?></p>
								<br>
							</li>
						<?php endforeach; ?>
					<?php endif; ?>
				</ul>
			</div>

		</div>
	</div>
</div>
