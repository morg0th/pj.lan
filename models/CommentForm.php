<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Class CommentForm
 * @package app\models
 * @property integer $article_id
 * @property string  $email
 * @property string  $user
 * @property string  $text
 * @property string  $verifyCode
 */
class CommentForm extends Model {

	public $article_id;

	public $user;

	public $email;

	public $text;

	public $verifyCode;

	/**
	 * @return array the validation rules.
	 */
	public function rules() {
		return [
			[['email', 'user', 'text'], 'required'],
			[['text'], 'string'],
			[['email', 'user'], 'string', 'max' => 255],
			['user', 'unique', 'message' => 'This user already exists.'],
			['email', 'unique', 'message' => 'This email already exists.'],
			['email', 'email'],
			['verifyCode', 'captcha'],
		];
	}

	/**
	 * @return array customized attribute labels
	 */
	public function attributeLabels() {
		return [
			'verifyCode' => 'Verification Code',
		];
	}

	/**
	 * Sends an email to the specified email address using the information collected by this model.
	 * @param string $email the target email address
	 * @return boolean whether the model passes validation
	 */
	public function contact($email) {
		if ($this->validate()) {
			Yii::$app->mailer->compose()->setTo($email)->setFrom([$this->email => $this->name])->setSubject($this->subject)->setTextBody($this->body)->send();

			return true;
		}
		return false;
	}

}
