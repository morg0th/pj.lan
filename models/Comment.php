<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comment".
 * @property integer $id
 * @property integer $article_id
 * @property string  $email
 * @property string  $user
 * @property string  $text
 */
class Comment extends \yii\db\ActiveRecord {

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'comment';
	}

	/**
	 * @param bool $insert
	 * @return bool
	 */
	public function beforeSave($insert) {
		$this->text = $this->replaceUrl($this->text);

		return parent::beforeSave($insert);
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['email', 'user', 'text'], 'required'],
			[['text'], 'string'],
			[['email', 'user'], 'string', 'max' => 255],

			['user', 'unique', 'message' => 'This user already exists.'],
			['email', 'unique', 'message' => 'This email already exists.'],

			['email', 'email'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'    => 'ID',
			'email' => 'Email',
			'user'  => 'User',
			'text'  => 'Text',
		];
	}

	/**
	 * Replace url in text
	 * @param string $inText
	 * @return string
	 */
	private function replaceUrl($inText){
		$string = preg_replace('/\b(?:(http(s?):\/\/)|(?=www\.))(\S+)/is', '<a href="http$2://$3" target="_blank">$1$3</a>', $inText);
		return $string;
	}

}
