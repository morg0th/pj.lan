<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "article".
 * @property integer $id
 * @property string  $title
 * @property string  $text
 */
class Article extends \yii\db\ActiveRecord {

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'article';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['text'], 'string'],
			[['title'], 'string', 'max' => 255],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'title' => 'Title',
			'text'  => 'Text',
		];
	}

}
