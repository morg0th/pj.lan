<?php

use yii\db\Migration;
use yii\db\Schema;

class m160623_143624_comment extends Migration {

	public function up() {
		$this->createTable('comment', [
			'id'         => Schema::TYPE_PK,
			'article_id' => $this->integer(11),
			'email'      => $this->string(255)->notNull(),
			'user'       => $this->string(255)->notNull(),
			'text'       => Schema::TYPE_TEXT,
		]);

		$this->createIndex('email', 'comment', 'email', true);
		$this->createIndex('user', 'comment', 'user', true);

		$this->addForeignKey('fk-article_id', 'comment', 'article_id', 'article', 'id', 'CASCADE');
	}

	public function down() {
		$this->dropForeignKey('fk-article_id', 'comment');

		$this->dropIndex('email', 'comment');
		$this->dropIndex('user', 'comment');

		$this->dropTable('comment');
	}

}
