<?php

use yii\db\Migration;
use yii\db\Schema;

class m160623_143611_article extends Migration {

	public function up() {
		$this->createTable('article', [
			'id'    => Schema::TYPE_PK,
			'title' => $this->string(255)->notNull(),
			'text'  => Schema::TYPE_TEXT,
		]);

		$this->insert('article', [
			'title'   => 'About Yii',
			'text'    => '<h2>What is Yii <span id="what-is-yii"></span></h2>
				Yii is a high performance, component-based PHP framework for rapidly developing modern Web applications. The name Yii (pronounced <code>Yee</code> or <code>[ji:]</code>) means "simple and evolutionary" in Chinese. It can also be thought of as an acronym for <strong>Yes It Is</strong>!
				<h2>What is Yii Best for? <span id="what-is-yii-best-for"></span></h2>
				Yii is a generic Web programming framework, meaning that it can be used for developing all kinds of Web applications using PHP. Because of its component-based architecture and sophisticated caching support, it is especially suitable for developing large-scale applications such as portals, forums, content management systems (CMS), e-commerce projects, RESTful Web services, and so on.
				<h2>How does Yii Compare with Other Frameworks? <span id="how-does-yii-compare-with-other-frameworks"></span></h2>
				If you\'re already familiar with another framework, you may appreciate knowing how Yii compares:
				<ul>
					<li>Like most PHP frameworks, Yii implements the MVC (Model-View-Controller) architectural pattern and promotes code organization based on that pattern.</li>
					<li>Yii takes the philosophy that code should be written in a simple yet elegant way. Yii will never try to over-design things mainly for the purpose of strictly following some design pattern.</li>
					<li>Yii is a full-stack framework providing many proven and ready-to-use features: query builders and ActiveRecord for both relational and NoSQL databases; RESTful API development support; multi-tier caching support; and more.</li>',
		]);
	}

	public function down() {
		$this->dropTable('article');
	}

}
