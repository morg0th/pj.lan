<?php

namespace app\controllers;

use Yii;
use app\models\Article;
use app\models\Comment;

class ArticleController extends \yii\web\Controller {

	public function actions() {
		return [
			'error'   => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class'           => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}

	public function actionIndex() {
		$article  = Article::find()->orderBy(['id' => SORT_DESC])->one();
		$comments = Comment::find()->where(['article_id' => $article->id])->orderBy(['id' => SORT_ASC])->all();

		return $this->render('index', [
			'article'  => $article,
			'comments' => $comments,
		]);
	}

	public function actionComment() {
		$errors        = null;
		$returnComment = null;
		$commentData   = Yii::$app->request->post('CommentForm');

		$comment             = new Comment();
		$comment->article_id = (int)$commentData['article_id'];
		$comment->email      = $commentData['email'];
		$comment->user       = $commentData['user'];
		$comment->text       = $commentData['text'];
		if ($comment->validate()) {
			if ($comment->save()) {
				$returnCommentData = Comment::findOne(['id' => $comment->getPrimaryKey()]);
				$returnComment     = [
					'id'         => $returnCommentData->id,
					'article_id' => $returnCommentData->article_id,
					'user'       => $returnCommentData->user,
					'email'      => $returnCommentData->email,
					'text'       => $returnCommentData->text,
				];
			}

		} else {
			$errors = $comment->getErrors();
		}

		return json_encode([
			'comment' => $returnComment,
			'errors'  => $errors,
		]);

	}

}
